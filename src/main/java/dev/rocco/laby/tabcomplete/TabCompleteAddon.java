package dev.rocco.laby.tabcomplete;

import net.labymod.api.LabyModAddon;
import net.labymod.settings.elements.BooleanElement;
import net.labymod.settings.elements.ControlElement;
import net.labymod.settings.elements.SettingsElement;
import net.labymod.utils.Material;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiChat;
import net.minecraft.command.CommandBase;

import java.lang.reflect.Field;
import java.util.List;
import java.util.stream.Collectors;

public class TabCompleteAddon extends LabyModAddon {

    private static Field waitingForTab;
    private static TabCompleteAddon INSTANCE;
    private boolean enabled, commands;

    @Override
    public void onEnable() {
        INSTANCE = this;
        Class<GuiChat> chat = GuiChat.class;
        try {
            waitingForTab = chat.getDeclaredField("field_146414_r");
        } catch (NoSuchFieldException e) {
            try {
                waitingForTab = chat.getDeclaredField("r");
            } catch (NoSuchFieldException ex) {
                throw new RuntimeException("Couldn't find the tab complete waiting field.", ex);
            }
        }
        waitingForTab.setAccessible(true);
    }

    @Override
    public void onDisable() {

    }

    @Override
    public void loadConfig() {
        enabled = !getConfig().has("enabled") || getConfig().get("enabled").getAsBoolean();
        commands = getConfig().has("commands") && getConfig().get("commands").getAsBoolean();
    }

    @Override
    protected void fillSettings(List<SettingsElement> list) {
        list.add(new BooleanElement("Enabled", this, new ControlElement.IconData(Material.LEVER), "enabled", true));
        list.add(new BooleanElement("Instant commands", this, new ControlElement.IconData(Material.LEVER), "commands", false));
    }

    public static boolean applyTabCompletion(String beforeCursor, String word) {
        if(!INSTANCE.enabled) return true;
        if(beforeCursor == null || beforeCursor.isEmpty()) return true;
        if(beforeCursor.charAt(0) == '/' && !INSTANCE.commands) return true;
        if(Minecraft.getMinecraft().currentScreen instanceof GuiChat) {
            try {
                waitingForTab.set(Minecraft.getMinecraft().currentScreen, true);
            }
            catch(Exception e) {
                e.printStackTrace();
            }
            ((GuiChat)Minecraft.getMinecraft().currentScreen)
                    .onAutocompleteResponse(CommandBase.getListOfStringsMatchingLastWord(beforeCursor.split(" "),
                            Minecraft.getMinecraft().getNetHandler().getPlayerInfoMap().stream()
                                    .map(p -> p.getDisplayName() == null ? p.getGameProfile().getName() : p.getDisplayName().getUnformattedText()).collect(Collectors.toList()))
                            .toArray(new String[0]));
            return false;
        }
        return true;
    }
}
