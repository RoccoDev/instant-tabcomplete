package dev.rocco.laby.tabcomplete.asm;

import net.labymod.core.asm.global.ClassEditor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.*;

public class ChatVisitor extends ClassEditor {
    public ChatVisitor() {
        super(ClassEditorType.CLASS_NODE);
    }

    @Override
    public void accept(String name, ClassNode node) {
        for(MethodNode method : node.methods) {
            if("a".equals(method.name) && "(Ljava/lang/String;Ljava/lang/String;)V".equals(method.desc)) {
                InsnList list = new InsnList();
                LabelNode label = new LabelNode();
                boolean labelFound = false;
                for(AbstractInsnNode instruction : method.instructions.toArray()) {
                    list.add(instruction);
                    if(instruction.getOpcode() == Opcodes.IF_ICMPLT || (instruction instanceof LabelNode && !labelFound)) {
                        labelFound = addInsns(list, label);
                    }
                }
                list.add(label);
                list.add(new InsnNode(Opcodes.RETURN));
                method.instructions.clear();
                method.instructions.insert(list);
            }
        }
    }

    private boolean addInsns(InsnList list, LabelNode label) {
        list.add(new VarInsnNode(Opcodes.ALOAD, 1));
        list.add(new VarInsnNode(Opcodes.ALOAD, 2));
        list.add(new MethodInsnNode(Opcodes.INVOKESTATIC,
                "dev/rocco/laby/tabcomplete/TabCompleteAddon", "applyTabCompletion", "(Ljava/lang/String;Ljava/lang/String;)Z"));
        list.add(new JumpInsnNode(Opcodes.IFEQ, label));
        return true;
    }
}
